package kz.aitu.oop.quiz.quiz7;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
    Scanner scanInput = new Scanner(System.in);
    FoodFactory foodFactory = new FoodFactory();

    Food food = foodFactory.getFood(scanInput.nextLine());

    System.out.println("The factory returned " + food.getClass());
    System.out.println(food.getType());
}
}
