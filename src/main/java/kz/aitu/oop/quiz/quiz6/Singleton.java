package kz.aitu.oop.quiz.quiz6;

public class Singleton {
    public String str;

    public static Singleton instance = null;

    private Singleton() { }

    public static Singleton getSingleInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        System.out.println("Hello! I an a Singleton! Let me say hello to you!");
        return instance;
    }
}
