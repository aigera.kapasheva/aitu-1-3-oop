package kz.aitu.oop.examples.assignment7.task3;

public interface GeometricObject {

    public abstract double getArea();
    public abstract double getPerimeter();
}
