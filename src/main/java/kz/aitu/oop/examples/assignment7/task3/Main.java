package kz.aitu.oop.examples.assignment7.task3;

public class Main {

    public static void main(String[] args) {
       Circle TestCircle = new Circle();
        System.out.println(TestCircle);
        System.out.println(TestCircle.getArea());
        System.out.println(TestCircle.getPerimeter());
        System.out.println(TestCircle.toString());

       ResizableCircle TestResizableCircle = new ResizableCircle(10);
        System.out.println(TestResizableCircle);
        System.out.println(TestResizableCircle.getArea());
        System.out.println(TestResizableCircle.getPerimeter());
        System.out.println(TestResizableCircle.toString());
        System.out.println(TestResizableCircle.resize(20));
    }
}
