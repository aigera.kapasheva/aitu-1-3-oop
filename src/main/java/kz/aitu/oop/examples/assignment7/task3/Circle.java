package kz.aitu.oop.examples.assignment7.task3;

public class Circle implements GeometricObject {

    public double radius = 1.0;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {

    }

    @Override
    public double getArea() {
        return 3.14 * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return 6.28 * radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }
}
