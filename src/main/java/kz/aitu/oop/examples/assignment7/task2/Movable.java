package kz.aitu.oop.examples.assignment7.task2;

public interface Movable {

    public abstract void moveUp();

    public abstract void moveDown();

    public abstract void moveLeft();

    public abstract void moveRight();
}
