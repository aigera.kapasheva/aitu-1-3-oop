package kz.aitu.oop.examples.practice5.task2;
import java.io.File;
import java.util.Scanner;
public class main {
    public static void main(String[] args) {
        String a = "12.8";
        System.out.println(fileExists("C:/Users/Айка/Downloads/Practice%205%20(1).pdf"));
        System.out.println(isInt(a));
        System.out.println(isDouble(a));
    }

    public static boolean fileExists(String path){
        try {
            File file = new File(path);
            Scanner sc = new Scanner(file);
        } catch (Exception e){
            return false;
        }
        return true;
    }


    public static boolean isInt(String a) {
        try {
            int number = Integer.parseInt(a);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public static boolean isDouble(String a) {
        try {
            double doubleNumber = Double.parseDouble(a);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
